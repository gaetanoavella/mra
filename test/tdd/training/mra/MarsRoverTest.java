package tdd.training.mra;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/*
//It initializes the rover at the coordinates (0,0), facing North, on a
//10x12 planet with obstacles at the coordinates (5,5) and (7,8).

List<String> planetObstacles = new ArrayList<>();
planetObstacles.add("(5,5)");
planetObstacles.add("(7,8)");
MarsRover rover = new MarsRover(10, 12, planetObstacles);

//It determinates whether, or not, the planet contains an obstacle in a
cell.

boolean obstacle = rover.planetContainsObstacleAt(7, 8);

//It lets the rover move on the planet according to a command string. The
//return string contains the new position of the rover, its direction,
and the obstacles it has encountered while moving on the planet (if any).

String commandString = "f";
String returnString = rover.executeCommand(commandString);
*/

public class MarsRoverTest {

	@Test
	public void testPlanetObstacles() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
	
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		
		assertTrue (rover.planetContainsObstacleAt(4,7));
		assertTrue (rover.planetContainsObstacleAt(2,3));
	}

	@Test
	public void testLanding() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = ("");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(0);
		rover.setPositionY(0);
		rover.setDirection("N");

		assertEquals ("(0,0,N)", rover.executeCommand(commandString) );
	}

	
	@Test
	public void testTurningRight() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "r";
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(0);
		rover.setPositionY(0);
		rover.setDirection("N");
		
		
		assertEquals ("(0,0,E)", rover.executeCommand(commandString) );
	}
	
	
	@Test
	public void testTurningLeft() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "l";
		
		MarsRover rover = new MarsRover(0, 0, planetObstacles);
		rover.setPositionX(0);
		rover.setPositionY(0);
		rover.setDirection("N");
		
		assertEquals ("(0,0,W)", rover.executeCommand(commandString) );
	}

	
	
	@Test
	public void testMovingForward() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "f";
		
		MarsRover rover = new MarsRover(0, 0, planetObstacles);	
		rover.setPositionX(7);
		rover.setPositionY(6);
		rover.setDirection("N");
		
		assertEquals ("(7,7,N)", rover.executeCommand(commandString) );
	}
	
	@Test
	public void testMovingBackwardAndDirectionIsEst() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "b";
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection("E");
		assertEquals ("(4,8,E)", rover.executeCommand(commandString) );
	}
	
	@Test
	public void testMovingBackwardAndDirectionIsSud() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "b";
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection("S");
		assertEquals ("(5,7,S)", rover.executeCommand(commandString) );
	}
	
	
	
	@Test
	public void testMovingCombined() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "ffrff";
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(0);
		rover.setPositionY(0);
		rover.setDirection("N");
		
		assertEquals ("(2,2,E)", rover.executeCommand(commandString) );
	}
	
	/* test method for US7
	@Test
	public void testWrapping() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		String commandString = "b";
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		rover.setPositionX(0);
		rover.setPositionY(0);
		rover.setDirection("N");
		
		assertEquals ("(0,9,N)", rover.executeCommand(commandString) );
	}
     */

	

	
}
