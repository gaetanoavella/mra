package tdd.training.mra;

import java.util.List;

public class MarsRover {

	private int planetX;
	private int planetY;
	private int positionX;
	private int positionY;
	private List<String> planetObstacles;
	private String direction;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		if (! planetObstacles.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		int positionX= getPositionX();
		int positionY = getPositionY();
		String direction= getDirection();
		
			
		if (commandString =="") {	
			positionX=0;
			positionY=0;
		}
	
		for(int i=0;i<commandString.length();i++) {
			char c = commandString.charAt(i);

			if (c == 'r') {	
				direction = "E";
		
			}else if (c=='l') {
			direction="W";
		
		
			} else if (c =='f' && direction == "N") {
				positionY = (positionY+1);
			} else if (c =='f' && direction == "E") {
				positionX = (positionX+1);
	
			} else if (c == 'b' && direction == "S") {
				positionY = (positionY-1);
			} else if (c == 'b' && direction == "E") {
				positionX = (positionX-1);	
		}
		}
		return ("("+positionX +","+ positionY +","+ direction +")");
	}
	
	public int getPositionX() {
		return positionX;
	}
	
    public void setPositionX(int positionX) {
		this.positionX = positionX;
	}
	
	public int getPositionY() {
		return positionY;
	}
	
	public void setPositionY(int positionY) {
			this.positionY = positionY;
		}
	 
	public String getDirection() {
			return direction;
		}
		
	public void setDirection(String direction) {
				this.direction = direction;
			}

}
